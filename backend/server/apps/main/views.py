from rest_framework.views import APIView
from .serializers import PostSerializer
from rest_framework.response import Response
from .models import Post
import os


class NewPost(APIView):

    def post(self,request):
        serializer = PostSerializer(data = request.data)
        if not serializer.is_valid():
            return Response("Data is not valid",403)
        serializer.save()
        return Response("Post has beed saved",200)


class GetPosts(APIView):

    def get(self,request):
        posts = Post.objects.all()
        data = PostSerializer(posts, many=True).data
        return Response(data,200)


class GetServerId(APIView):
    
    def get(self,request):
        return Response(os.environ.get('ID'),200)