from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=100,blank=False)
    content = models.TextField(blank=False)
    publish_date = models.DateTimeField(auto_now=True)