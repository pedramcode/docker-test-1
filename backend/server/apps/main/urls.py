from django.urls import path
from .views import NewPost,GetPosts,GetServerId

urlpatterns = [
    path('new/', NewPost.as_view()),
    path('all/', GetPosts.as_view()),
    path('id/', GetServerId.as_view()),
]
